using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.ResponseCaching;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using b2b.Model;
using b2b.Model.Data;
using b2b.Model.Data.Repositories;
using b2b.Model.Request;
using b2b.Model.Response;
using System.Collections.Concurrent;
using static b2b.Model.Data.Repositories.Repository;
using b2b.Helper;
using b2b.Model.Data.Enums;
using SystemExtension;

namespace b2b.Controllers
{
    public class ServicesController : Controller
    {
        IRepository repository;
        IConfiguration configuration;
        IMemoryCache memoryCache;
        IWebHostEnvironment hostingEnvironment;

        /// SearchHotelsResponseStorage storage;
        public ServicesController(IRepository r, IConfiguration c, IMemoryCache memoryCache, IWebHostEnvironment hostingEnvironment)
        {
            repository = r;
            configuration = c;
            this.memoryCache = memoryCache;
            this.hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }

        /// <summary>
        /// ����� ������.
        /// </summary>
        /// <param name="searchParams"></param>
        /// <param name="lng"></param>
        /// <returns></returns>
        [Authorize(Roles = "SYSDBA_WebAis, SITE_FULL, SITE_VIEW"), HttpPost]
        [Route("hotelSearch/{lng}")]
        [ResponseCache(VaryByHeader = "User-Agent", Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task hotelSearch([FromBody] SearchHotelRequest searchParams, string lng)
        {
            try
            {
                if (searchParams == null)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    await Response.WriteAsync("searchParams is null");
                    return;
                }
                var userDetails = CommonHelper.GetKagentDetailsFromToken(Request);
                var makeForTariff = CalculateHelper.make_for_tarif(CommonHelper.GetClaimFromToken(Request, ClaimEnum.CabinetUser));
                var result = hotelSearch(searchParams, lng, userDetails, makeForTariff);
                Response.ContentType = "application/json";


                //var responseCachingFeature = Response.HttpContext.Features.Get<IResponseCachingFeature>();
                //if (responseCachingFeature != null)
                //{
                //    responseCachingFeature.VaryByQueryKeys = new[] { "MyKey" };
                //}


                await Response.WriteAsync(JsonHelper.ObjectToJsonLowerCamelCase(result));
            }
            catch (Exception ex)
            {
                var logger = ApplicationLogging.CreateLogger(LoggerType.Error);
                logger.LogError(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(CommonHelper.GetKagentDetailsFromToken(Request).IdUser, ex.Message)));
                await Response.WriteAsync(ex.Message);
            }
        }

        
        /// <summary>
        /// ����� ������.
        /// </summary>
        /// <param name="searchParams"></param>
        /// <param name="lng"></param>
        /// <param name="userDetails"></param>
        /// <param name="makeForTariff"></param>
        /// <returns></returns>
        public SearchHotelResponse hotelSearch(SearchHotelRequest searchParams, string lng, KagentDetails userDetails, string makeForTariff)
        {
            var logStr = "";
            try
            {
                var discounts = repository.GetListDiscountService(searchParams.checkIn, SegmentType.BASKET_TYPE_HOTEL.Description(), userDetails.IdKagent);
                Dictionary<decimal, SearchHotelResponse.Hotel> hotels;
                Dictionary<SearchHotelResponse.HotelService, SearchHotelResponse.Service> services;
                Dictionary<HotelServiceService, AdditionalServicesResponse.Service> additionalServicesData = new Dictionary<HotelServiceService, AdditionalServicesResponse.Service>();
                Dictionary<SearchHotelResponse.HotelService, List<AdditionalServicesResponse.Service>> additionalServicesCalculated = new Dictionary<SearchHotelResponse.HotelService, List<AdditionalServicesResponse.Service>>();
                if (searchParams.hotelList == null)
                    searchParams.hotelList = new List<int> { 0 };
                if (!searchParams.backgroundRequest)
                {
                    var searchResult = SearchInTask(searchParams, lng, userDetails, makeForTariff);
                    hotels = searchResult.Item1;
                    services = searchResult.Item2;
                    additionalServicesData = searchResult.Item3;
                    additionalServicesCalculated = CalcAdditionalServices(searchParams.checkIn, searchParams.checkOut, userDetails.IdKagent, services, additionalServicesData, discounts);
                }
                else
                {
                    var searchResult = repository.HotelSearch(CultureInfo.GetCultureInfo(lng), userDetails, searchParams.backgroundRequest, searchParams.checkIn, searchParams.checkOut, searchParams.city, searchParams.hotelList, makeForTariff, searchParams.stars, searchParams.single, searchParams.@double, hostingEnvironment);
                    hotels = searchResult.Item1;
                    services = searchResult.Item2;
                }

                //����������� ������ � ������
                foreach (var srv in services)
                {
                    logStr = "idHotel = " + srv.Key.idHotel + ", idService = " + srv.Key.idService;
                    if (!hotels.ContainsKey(srv.Key.idHotel))
                        continue;


                    decimal ob_price = 0;
                    if (!searchParams.backgroundRequest)
                    {
                        if (!additionalServicesCalculated.ContainsKey(srv.Key))
                            continue;
                        var additionalServiceResponse = additionalServicesCalculated[srv.Key];
                        foreach (var additionalSrv in additionalServiceResponse)
                        {
                            var fillDays = CalculateHelper.FillAllDays(additionalSrv.Tariffs, searchParams.checkIn, searchParams.checkOut);
                            if (additionalSrv.Included)
                            {
                                if (fillDays == false)
                                    goto CONTINUE;//�� ��������� �������� ������ ���� � ������������ ���. ������ ��� �������
                                var objAdditionalSrvDiscount = CalculateHelper.GetDiscountPercent(discounts, searchParams.checkIn, additionalSrv.Type, additionalSrv.BaseType, additionalSrv.SubBaseType, userDetails.IdKagent, 1, additionalSrv.CityBeginId, additionalSrv.ObjectBeginId, additionalSrv.CityEndId, additionalSrv.ObjectEndId);
                                var discountAdditionalService = objAdditionalSrvDiscount == null ? 0 : objAdditionalSrvDiscount.PercentDiscount;
                                ob_price = ob_price + additionalSrv.Price[0] + additionalSrv.Price[0] * discountAdditionalService / 100; //�������� ��������� ��������� ������������ ���.����� (����� ��������� �� � ��������� ���. ������)
                            }
                        }
                        ob_price = Math.Ceiling(ob_price);
                        srv.Value.BreakfastInclude = additionalServiceResponse.Any(s => s.Breakfast && s.Included && s.IsActive);
                        srv.Value.ExistAdditionalService = additionalServiceResponse.Where(s => !s.Included || !s.Breakfast).Count() > 0;
                    }
                    var calcType = hotels[srv.Key.idHotel].sourceEnum.GetCalcType();
                    var srvTariffs = srv.Value.Tariffs;
                    var discount = CalculateHelper.GetDiscountPercent(discounts, searchParams.checkIn, srv.Value.Type, srv.Value.BaseType, srv.Value.SubBaseType, userDetails.IdKagent, 1, srv.Value.CityBeginId, srv.Value.ObjectBeginId, srv.Value.CityEndId, srv.Value.ObjectEndId).PercentDiscount;
                    var calcTariff = CalculateHelper.CalculateTariff(srvTariffs, calcType, searchParams.checkIn, searchParams.checkOut, ob_price, discount);
                    srv.Value.AveragePrice = calcTariff.AverageTariff;
                    srv.Value.TotalPrice = calcTariff.SumTariff;
                    srv.Value.UnitOfPaymentEnum = calcTariff.UnitOfPayment;
                    if (calcTariff.SumTariff > 0 && hotels.ContainsKey(srv.Key.idHotel))
                        hotels[srv.Key.idHotel].services.Add(srv.Value);
                    CONTINUE:;
                }

                //���������� �������
                foreach (var hotel in hotels)
                    hotel.Value.services = hotel.Value.services.OrderBy(s => s.NonRefund).ThenBy(s => s.TotalPrice).ToList();

                var hotelsResponse = new SearchHotelResponse();
                hotelsResponse.Hotels.AddRange(hotels.Values);
                return hotelsResponse;
            }
            catch (Exception ex)
            {
                var name = new StackTrace(false).GetFrame(0).GetMethod().Name;
                var logger = ApplicationLogging.CreateLogger(LoggerType.Error);
                logger.LogError(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(0, name + " error, " + logStr + Environment.NewLine + ex.Message)));
                return new SearchHotelResponse();
            }
        }

        Tuple<Dictionary<decimal, SearchHotelResponse.Hotel>, Dictionary<SearchHotelResponse.HotelService, SearchHotelResponse.Service>, Dictionary<HotelServiceService, AdditionalServicesResponse.Service>> SearchInTask(SearchHotelRequest searchParams, string lng, KagentDetails userDetails, string makeForTariff)
        {
            try
            {
                makeForTariff = userDetails.Role == UserRole.SITE_VIEW ? "FIT_EURO" : makeForTariff;
                Task<Tuple<Dictionary<decimal, SearchHotelResponse.Hotel>, Dictionary<SearchHotelResponse.HotelService, SearchHotelResponse.Service>>> result;
                Task<Dictionary<HotelServiceService, AdditionalServicesResponse.Service>> additionalServices; ;

                var task1 = Task.Factory.StartNew(() =>
                    repository.HotelSearch(CultureInfo.GetCultureInfo(lng), userDetails, searchParams.backgroundRequest, searchParams.checkIn, searchParams.checkOut, searchParams.city, searchParams.hotelList, makeForTariff, searchParams.stars, searchParams.single, searchParams.@double, hostingEnvironment));
                string curfirmtek = userDetails.CurrencyCodeFirm;
                string typeBlockFirm = CommonHelper.GetTypeBlock(curfirmtek, userDetails.Role);
                curfirmtek = curfirmtek == "" ? userDetails.CurrencyCode : curfirmtek;
                var task2 = Task.Factory.StartNew(() =>
                    repository.GetAdditionalServices3(CultureInfo.GetCultureInfo(lng), userDetails, searchParams.checkIn, searchParams.checkOut, searchParams.city, searchParams.hotelList, curfirmtek, makeForTariff, typeBlockFirm));
                result = task1;
                additionalServices = task2;
                Task.WaitAll(task1, task2);
                return new Tuple<Dictionary<decimal, SearchHotelResponse.Hotel>, Dictionary<SearchHotelResponse.HotelService, SearchHotelResponse.Service>, Dictionary<HotelServiceService, AdditionalServicesResponse.Service>>(result.Result.Item1, result.Result.Item2, additionalServices.Result);
            }
            catch (Exception ex)
            {
                var name = new StackTrace(false).GetFrame(0).GetMethod().Name;
                var logger = ApplicationLogging.CreateLogger(LoggerType.Error);
                logger.LogError(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(CommonHelper.GetKagentDetailsFromToken(Request).IdUser, name + " error, " + Environment.NewLine + ex.Message, false, new Dictionary<string, object> { { "StackTrace", ex.StackTrace } })));
                return null;
            }
        }

        [Authorize(Roles = "SYSDBA_WebAis, SITE_FULL, SITE_VIEW"), HttpPost]
        [Route("taskHotelSearchBegin/{lng}")]
        public async Task taskHotelSearchBegin([FromBody] TaskHotelSearchBeginRequest searchParams, string lng)
        {
            var paramsDicionary = new Dictionary<string, object>();
            if (SearchHotelsResponseStorage.storage == null)
                SearchHotelsResponseStorage.CreateStorage(configuration);
            var id = Guid.Empty;
            try
            {
                id = SearchHotelsResponseStorage.storage.GetTaskId();
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonHelper.ObjectToJsonLowerCamelCase(new { taskId = id }));

                var userDetails = CommonHelper.GetKagentDetailsFromToken(Request);
                var makeForTariff = CalculateHelper.make_for_tarif(CommonHelper.GetClaimFromToken(Request, ClaimEnum.CabinetUser));

                paramsDicionary.Add("city", searchParams.city);
                paramsDicionary.Add("hotelList", string.Join(",", searchParams.hotelList.Select(h => h)));
                paramsDicionary.Add("hotelType", searchParams.hotelType);
                paramsDicionary.Add("checkIn", searchParams.checkIn);
                paramsDicionary.Add("checkOut", searchParams.checkOut);
                paramsDicionary.Add("stars", string.Join(",", searchParams.stars.Select(h => h)));
                paramsDicionary.Add("breakfast", searchParams.breakfast);
                paramsDicionary.Add("single", searchParams.single);
                paramsDicionary.Add("double", searchParams.@double);
                paramsDicionary.Add("alternative", searchParams.alternative);
                paramsDicionary.Add("distance", searchParams.distance);
                paramsDicionary.Add("averagePrice", searchParams.averagePrice);
                paramsDicionary.Add("freeSale", searchParams.freeSale);
                paramsDicionary.Add("numberOfGuest", searchParams.numberOfGuest);
                paramsDicionary.Add("online", searchParams.online);
                var logger = ApplicationLogging.CreateLogger(LoggerType.Request);
                //logger.LogInformation(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(CommonHelper.GetKagentDetailsFromToken(Request).IdUser, "taskHotelSearchBegin", false, paramsDicionary)));

                Task.Run(() => HotelSearchHybrid(searchParams, lng, id, userDetails, makeForTariff));
            }
            catch (Exception ex)
            {
                SearchHotelsResponseStorage.storage.ClearStorage(id);
                var name = new StackTrace().GetFrames().Select(frame => frame.GetMethod()).FirstOrDefault(item => item.DeclaringType == GetType()).Name;
                var logger = ApplicationLogging.CreateLogger(LoggerType.Error);
                logger.LogError(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(CommonHelper.GetKagentDetailsFromToken(Request).IdUser, name + " error, " + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace, false, paramsDicionary)));
                await Response.WriteAsync(ex.Message);
            }
        }

        void HotelSearchHybrid(TaskHotelSearchBeginRequest searchParams, string lng, Guid taskId, KagentDetails userDetails, string makeForTariff)
        {
            try
            {
                var logger = ApplicationLogging.CreateLogger(LoggerType.Info);
                //logger.LogInformation("!!!! ----------- HotelSearchHybrid --------- taskId = " + taskId);

                DateTime d1 = DateTime.Now;

                Task<Dictionary<int, GeoCoordinatePortable.GeoCoordinate>> taskHotelCoordinates = null;
                if (searchParams.alternative)
                {
                    var objectController = new ObjectsController(repository, memoryCache, hostingEnvironment);
                    taskHotelCoordinates = Task.Factory.StartNew(() => objectController.objectCoordinate().cities.FirstOrDefault(c => c.id == searchParams.city).hotels.ToDictionary(key => (int)key.id, value => value.geoCoordinate));
                }
                var searchParamsAsync = new SearchHotelRequest(false, searchParams.checkIn, searchParams.checkOut, searchParams.city, searchParams.alternative ? new List<int>() : searchParams.hotelList, searchParams.alternative ? new List<int>() : searchParams.stars, searchParams.single, searchParams.@double, searchParams.alternative ? false : searchParams.online);
                var typeBlockFirm = CommonHelper.GetTypeBlock(userDetails.CurrencyCodeFirm);
                var listHotels = repository.GetExternalHotels(CultureInfo.GetCultureInfo(lng), searchParams.city, searchParamsAsync.hotelList, searchParams.stars, typeBlockFirm, hostingEnvironment);
                searchParamsAsync.backgroundRequest = false;
                var task1 = Task.Factory.StartNew(() => hotelSearch(searchParamsAsync, lng, userDetails, makeForTariff));
                var tasks = new List<SearchTask>();
                var extSystemsCount = 0;
                IEnumerable<HotelSource> hotelSources = null;
                //�������� ������� �����
                if (listHotels.Count > 0)
                {
                    hotelSources = listHotels.Values.GroupBy(h => h.HotelSource).Select(h => h.Key);
                    extSystemsCount = hotelSources.Count();
                    foreach (var source in hotelSources.OrderByDescending(s => s == HotelSource.sunhotels))
                    {
                        if (source == HotelSource.travelline)
                        {
                            continue;
                        }

                        var searchParamsAsync2 = new SearchHotelRequest(true, searchParams.checkIn, searchParams.checkOut, searchParams.city, listHotels.Where(h => h.Value.HotelSource == source).Select(h => h.Key).ToList(), searchParams.stars, searchParams.single, searchParams.@double, searchParams.online);
                        if (source == HotelSource.travelline2)
                        {
                            searchParamsAsync2.backgroundRequest = false;
                            makeForTariff = string.Empty;
                        }

                        var task2 = Task.Factory.StartNew(() => hotelSearch(searchParamsAsync2, lng, userDetails, makeForTariff));
                        tasks.Add(new SearchTask() { task = task2, source = source });
                    }
                }
                //1.��������� ���������� �����, ����� ��������� (������� �������� ����� �� ������� ��������)

                task1.Wait();

                var outResult = task1.Result.Hotels;
                outResult = CommonHelper.FilterHotels(searchParams, taskHotelCoordinates, outResult);
                var outResultUndefinedHotel = outResult.Where(h => h.ChildHotelType == HotelSource.undefined).ToList();
                SearchHotelsResponseStorage.storage.AddToStorage(taskId, CalcPercent(extSystemsCount / 2 + 1, extSystemsCount), outResultUndefinedHotel);

                //logger.LogInformation("storage count = " + SearchHotelsResponseStorage.storage.searchResponses.Count + " outResultUndefinedHotel.Count = " + outResultUndefinedHotel.Count + " taskId = " + taskId);

                DateTime d2 = DateTime.Now;

                if (!SearchHotelsResponseStorage.storage.searchResponses.ContainsKey(taskId))
                {
                    SearchHotelsResponseStorage.storage.AddToStorage(taskId, CalcPercent(extSystemsCount + 1, extSystemsCount), new List<SearchHotelResponse.Hotel>());
                }

                //2.��������� ��������� ����� (���������� ��������� � ��� �������)
                if (tasks.Count > 0)
                {
                    SearchHotelsResponseStorage.SearchResponse searchResponsesHybrid = new SearchHotelsResponseStorage.SearchResponse();
                    foreach (var task2 in tasks)
                    {
                        task2.task.Wait();
                        var outResult2 = ((Task<SearchHotelResponse>)task2.task).Result.Hotels;
                        outResult2 = CommonHelper.FilterHotels(searchParams, taskHotelCoordinates, outResult2);
                        searchResponsesHybrid.AddHotels(CalcPercent(extSystemsCount + 1, extSystemsCount), searchResponsesHybrid.LifeTime.AddSeconds(SearchHotelsResponseStorage.storage.searchResultTimeOutSec), outResult.Where(h => h.ChildHotelType.HasFlag(task2.source)).Concat(outResult2).ToList());
                    }

                    //logger.LogInformation("Hybrid hotels count = " + searchResponsesHybrid.Hotels.Count + " taskId = " + taskId);

                    if (SearchHotelsResponseStorage.storage.searchResponses.ContainsKey(taskId))
                    {
                        foreach (var hotel in searchResponsesHybrid.Hotels)
                        {
                            SearchHotelsResponseStorage.storage.searchResponses[taskId].Hotels.TryAdd(hotel.Value.Id, hotel.Value);

                        }
                    }
                    else
                    {
                        //logger.LogInformation("SearchHotelsResponseStorage.storage.searchResponses does not containe taskId = " + taskId);
                    }
                }
                else
                    SearchHotelsResponseStorage.storage.AddToStorage(taskId, 100, outResult.Where(h => h.ChildHotelType != HotelSource.undefined).ToList());



                DateTime d3 = DateTime.Now;

                double timeExecuteRequest = (d3 - d1).TotalSeconds;

                string logTimerSec = "Total time search hotels t = " + timeExecuteRequest + " ���. time ais hotels = " + (d2 - d1).TotalSeconds + " ���.";
                if (timeExecuteRequest > 10)
                {
                    CommonHelper.SaveLog(logTimerSec, userDetails.IdUser, searchParams, "");
                }

                /*
                string hotels = "\r\n";
                int i = 0;
                foreach (var hotel in SearchHotelsResponseStorage.storage.searchResponses[taskId].Hotels)
                {
                    hotels += "" + ++i + ". " + hotel.Value.Name + ", source = " + hotel.Value.sourceEnum + ", ChildHotelType = " + hotel.Value.ChildHotelType + "; \r\n";
                }
                logger.LogInformation(logTimerSec + " user id = " + userDetails.IdUser + "\r\nhotels = " + hotels);

                logger.LogInformation("total hotels counts = " + SearchHotelsResponseStorage.storage.searchResponses[taskId].Hotels.Count + " taskId = " + taskId);
                */
                SearchHotelsResponseStorage.storage.SetEof(taskId, true);
                //logger.LogInformation("Eof = " + SearchHotelsResponseStorage.storage.GetResponse(taskId).Eof + " taskId = " + taskId);
            }
            catch (Exception ex)
            {
                SearchHotelsResponseStorage.storage.SetEof(taskId, true);
                var name = new StackTrace(false).GetFrame(0).GetMethod().Name;
                var logger = ApplicationLogging.CreateLogger(LoggerType.Error);
                logger.LogError(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(userDetails.IdUser, name + " error, " + Environment.NewLine + ex.Message, false, new Dictionary<string, object> { { "StackTrace", ex.StackTrace } })));
            }
        }

        int CalcPercent(int taskNumber, int extSystemsCount)
        {
            return taskNumber * 100 / (extSystemsCount + 1);
        }

        public class SearchTask
        {
            public Task task;
            public HotelSource source;
        }

        [Authorize(Roles = "SYSDBA_WebAis, SITE_FULL, SITE_VIEW"), HttpPost]
        [Route("taskHotelSearchEnd/{taskId}")]
        public async Task taskHotelSearchEnd([FromBody] TaskHotelSearchEndRequest searchParams, Guid taskId)
        {
            try
            {
                Thread t = Thread.CurrentThread;
                var response = new TaskHotelSearchResponse() { metaData = new TaskHotelSearchResponse.MetaData() { eof = true, percent = 100 } };
                if (SearchHotelsResponseStorage.storage != null)
                {
                    var hotels = SearchHotelsResponseStorage.storage.GetHotels(taskId);
                    var i = 0;
                    var searchResult = SearchHotelsResponseStorage.storage.GetResponse(taskId);
                    var logger = ApplicationLogging.CreateLogger(LoggerType.Info);
                    //logger.LogInformation("taskHotelSearchEnd taskId =  " + taskId + " hotels count = " + hotels.Count + " searchParams.LastCount = " + searchParams.LastCount);
                    foreach (var hotel in hotels)
                    {
                        i++;
                        if (i > searchParams.LastCount)
                        {
                            response.hotels.Add(hotel);
                            //logger.LogInformation(i + ". " + hotel.Name);
                        }
                        else
                        {
                            //logger.LogInformation("-- " + i + ". " + hotel.Name);
                        }
                    }
                    response.metaData = new TaskHotelSearchResponse.MetaData()
                    {
                        lastCount = hotels.Count,
                        eof = searchResult == null ? false : searchResult.Eof,
                        percent = searchResult == null ? 5 : searchResult.Percent
                    };
                    Response.ContentType = "application/json";
                    await Response.WriteAsync(JsonHelper.ObjectToJsonLowerCamelCase(response));
                    if (searchResult != null && searchResult.Eof)
                        SearchHotelsResponseStorage.storage.ClearStorage(taskId);
                }
                else
                    await Response.WriteAsync(JsonHelper.ObjectToJsonLowerCamelCase(response));
            }
            catch (Exception ex)
            {
                SearchHotelsResponseStorage.storage.ClearStorage(taskId);
                var name = new StackTrace().GetFrames().Select(frame => frame.GetMethod()).FirstOrDefault(item => item.DeclaringType == GetType()).Name;
                var logger = ApplicationLogging.CreateLogger(LoggerType.Error);
                logger.LogError(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(CommonHelper.GetKagentDetailsFromToken(Request).IdUser, name + " error, " + Environment.NewLine + ex.Message, false, new Dictionary<string, object> { { "StackTrace", ex.StackTrace } })));
                await Response.WriteAsync(ex.Message);
            }
        }

        [Authorize(Roles = "SYSDBA_WebAis, SITE_FULL, SITE_VIEW"), HttpDelete]
        [Route("taskHotelSearchTerminate/{taskId}")]
        public async Task taskHotelSearchTerminate(Guid taskId)
        {
            try
            {
                var storage = SearchHotelsResponseStorage.storage;
                if (storage != null)
                    storage.ClearStorage(taskId);
                await Response.WriteAsync("Search results deleted");
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                await Response.WriteAsync("deleting search results error");
                var name = new StackTrace().GetFrames().Select(frame => frame.GetMethod()).FirstOrDefault(item => item.DeclaringType == GetType()).Name;
                var logger = ApplicationLogging.CreateLogger(LoggerType.Error);
                logger.LogError(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(CommonHelper.GetKagentDetailsFromToken(Request).IdUser, name + " error, " + Environment.NewLine + ex.Message, false,
                            new Dictionary<string, object>
                            {
                                { "StackTrace",  ex.StackTrace }
                            })));
                await Response.WriteAsync(ex.Message);
            }
        }

        /// <summary>
        /// ���������� �������������� ������
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "SYSDBA_WebAis, SITE_FULL, SITE_VIEW"), HttpPost]
        [Route("additionalServices/{lng}")]
        public async Task additionalServicesTask([FromBody] AdditionalServicesRequest request, string lng)
        {
            try
            {
                var userDetails = CommonHelper.GetKagentDetailsFromToken(Request);
                var additionalServicesCalulated = additionalServices(request, lng, userDetails);
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonHelper.ObjectToJsonLowerCamelCase(additionalServicesCalulated));
            }
            catch (Exception ex)
            {
                var logger = ApplicationLogging.CreateLogger(LoggerType.Error);
                logger.LogError(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(CommonHelper.GetKagentDetailsFromToken(Request).IdUser, ex.Message)));
                await Response.WriteAsync(ex.Message);
            }
        }

        /// <summary>
        /// ���������� �������������� ������
        /// </summary>
        /// <returns></returns>
        public AdditionalServicesResponse additionalServices([FromBody] AdditionalServicesRequest request, string lng, KagentDetails userDetails)
        {
            try
            {
                DateTime checkIn = request.checkIn;
                DateTime checkOut = request.checkOut;
                request.service = CommonHelper.GetServiceIdFromString(request.service);

                var service = repository.GetBaseService(userDetails, request.service, checkIn, checkOut);
                var key = new SearchHotelResponse.HotelService(service.ObjectBeginId, request.service);
                var baseServices = new Dictionary<SearchHotelResponse.HotelService, SearchHotelResponse.Service>();
                baseServices.Add(key, service);
                var discounts = repository.GetListDiscountService(checkIn, service.Type, userDetails.IdKagent); //�������� ������
                var additionalServicesData = repository.GetAdditionalServices(CultureInfo.GetCultureInfo(lng), userDetails, baseServices, checkIn, checkOut, discounts);
                var hotel = repository.GetObjectInfo(CultureInfo.GetCultureInfo(lng), (int)key.idHotel, 0, true);
                var additionalServicesCalulated = CalcAdditionalServices(checkIn, checkOut, userDetails.IdKagent, baseServices, additionalServicesData, discounts)[key];
                additionalServicesCalulated = additionalServicesCalulated.Where(s => s.IsActive).ToList();//�������� ������ �������� ������
                return new AdditionalServicesResponse(additionalServicesCalulated);
            }
            catch (Exception ex)
            {
                var logger = ApplicationLogging.CreateLogger(LoggerType.Error);
                logger.LogError(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(CommonHelper.GetKagentDetailsFromToken(Request).IdUser, ex.Message)));
                return new AdditionalServicesResponse();
            }
        }

        /// <summary>
        /// ������������ �������� ���. �����.
        /// </summary>
        /// <param name="checkIn">���� ������ ������.</param>
        /// <param name="checkOut">���� ��������� ������.</param>
        /// <param name="idKagent">������������� �����-��������.</param>
        /// <param name="baseServices">������ �����, ��� ������� ������������� ��������� ���.�����.</param>
        /// <param name="additionalServices">������ ���. �����, ��� ������� ����� ���������� ���������.</param>
        /// <param name="discounts">������ ���� ������</param>
        /// <param name="hotels">������ ������, �� ������� �������������� ��������� ���. �����.</param>
        /// <returns></returns>
        public static Dictionary<SearchHotelResponse.HotelService, List<AdditionalServicesResponse.Service>> CalcAdditionalServices(DateTime checkIn, DateTime checkOut, decimal idKagent, Dictionary<SearchHotelResponse.HotelService, SearchHotelResponse.Service> baseServices, Dictionary<HotelServiceService, AdditionalServicesResponse.Service> additionalServices, List<DiscountService> discounts)
        {
            Dictionary<string, object> errorParams = null;
            var response = new Dictionary<SearchHotelResponse.HotelService, List<AdditionalServicesResponse.Service>>();
            var logStr = "";
            try
            {
                foreach (var item in baseServices.Keys)
                    response.Add(item, new List<AdditionalServicesResponse.Service>());

                foreach (var baseSrvKey in baseServices.Keys)
                {


                    logStr = "idHotel = " + baseSrvKey.idHotel + ", idService = " + baseSrvKey.idService;
                    var calcType = CalcType.daily;
                    var objDiscount = CalculateHelper.GetDiscountPercent(discounts, checkIn, baseServices[baseSrvKey].Type, baseServices[baseSrvKey].BaseType, baseServices[baseSrvKey].SubBaseType, idKagent, 1, baseServices[baseSrvKey].CityBeginId, baseServices[baseSrvKey].ObjectBeginId, baseServices[baseSrvKey].CityEndId, baseServices[baseSrvKey].ObjectEndId);
                    var baseSrvDiscount = objDiscount == null ? 0 : objDiscount.PercentDiscount;
                    var baseSrvPrice = baseServices[baseSrvKey].AveragePrice + baseServices[baseSrvKey].AveragePrice * baseSrvDiscount / 100;
                    var hotelServices = additionalServices.Where(s => s.Key.hotelService.Equals(baseSrvKey));

                    errorParams = new Dictionary<string, object>(){
                        { "checkIn", checkIn },
                        { "checkOut", checkOut },
                        { "idKagent", idKagent},
                        { "baseService", baseServices[baseSrvKey]},
                        { "additionalServices", hotelServices},
                        { "discounts", discounts} };

                    decimal additionalBedTariffAverage = 0m;
                    foreach (var additionalBedService in hotelServices.Where(srv => srv.Value.AdditionalBed))
                    {
                        var sum = CalculateHelper.CalculateTariff(additionalBedService.Value.Tariffs, calcType, checkIn, checkOut, 0, 0);
                        additionalBedTariffAverage += sum.AverageTariff;
                    }
                    //������� ��� ������������ ������ � �������� �� ���������
                    decimal includedServicesTariffAverage = 0m;
                    foreach (var service in hotelServices.Where(srv => srv.Value.Included))
                    {
                        var sum = CalculateHelper.CalculateTariff(service.Value.Tariffs, calcType, checkIn, checkOut, 0, 0);
                        includedServicesTariffAverage += sum.AverageTariff;
                    }
                    //��������� ���������
                    var additionalBedCount = hotelServices.Where(srv => srv.Value.AdditionalBed).Count();
                    foreach (var service in hotelServices)
                    {
                        var objAdditionalSrvDiscount = CalculateHelper.GetDiscountPercent(discounts, checkIn, service.Value.Type, service.Value.BaseType, service.Value.SubBaseType, idKagent, 1, service.Value.CityBeginId, service.Value.ObjectBeginId, service.Value.CityEndId, service.Value.ObjectEndId);
                        var discount = objAdditionalSrvDiscount == null ? 0 : objAdditionalSrvDiscount.PercentDiscount;
                        service.Value.PriceList.Add(CalculateHelper.CalculateAdditionalServiceTariff(service.Value.Tariffs, baseServices[baseSrvKey].Tariffs, baseSrvDiscount, checkIn, checkOut, service.Value.UnitOfPaymentEnum, service.Value.TypeTariffEnum, baseSrvPrice, additionalBedTariffAverage, includedServicesTariffAverage, baseServices[baseSrvKey].Place, discount));
                        for (int i = 1; i <= additionalBedCount; i++)
                            service.Value.PriceList.Add(CalculateHelper.CalculateAdditionalServiceTariff(service.Value.Tariffs, baseServices[baseSrvKey].Tariffs, baseSrvDiscount, checkIn, checkOut, service.Value.UnitOfPaymentEnum, service.Value.TypeTariffEnum, baseSrvPrice, additionalBedTariffAverage, includedServicesTariffAverage, baseServices[baseSrvKey].Place + i, discount));
                        //�������� ������ ��� ����������, ���� � ������������ ������ ��� ����������� �������
                        if (service.Value.Included)
                        {
                            if (!CalculateHelper.FillAllDays(service.Value.Tariffs, checkIn, checkOut))
                                service.Value.IsActive = false;
                        }
                        if (service.Value.Included || service.Value.PriceList[0] > 0)
                            response[baseSrvKey].Add(service.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                var name = new StackTrace(false).GetFrame(0).GetMethod().Name;
                var logger = ApplicationLogging.CreateLogger(LoggerType.Error);
                logger.LogError(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(0, name + " error, " + logStr + Environment.NewLine + ex.Message, false, errorParams)));
            }
            return response;
        }

        /// <summary>
        /// ���������� ������� �������������� ����� ��� ������ �������� ������������
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "SYSDBA_WebAis, SITE_FULL, SITE_VIEW"), HttpPost]
        [Route("terms/{lng}")]
        public async Task terms([FromBody] TermsRequest request, string lng)
        {
            try
            {
                List<string> result = new List<string>();
                //var hotel = repository.GetObjectInfo(CultureInfo.GetCultureInfo(lng), request.HotelId, 0, true);
                if (!request.ServiceId.Contains('_'))
                {
                    CommonHelper.GetKagentDetailsFromToken(Request);
                    var makeForTariff = CalculateHelper.make_for_tarif(CommonHelper.GetClaimFromToken(Request, ClaimEnum.CabinetUser));
                    var term = repository.GetTermService(request.CheckIn, makeForTariff, int.Parse(request.ServiceId));
                    foreach (var msg in term.Split(Environment.NewLine).ToList())
                    {
                        if (!string.IsNullOrEmpty(msg))
                            result.Add(msg);
                    }
                }
                else
                {
                    var userDetails = CommonHelper.GetKagentDetailsFromToken(Request);
                    result = repository.GetTerms(CultureInfo.GetCultureInfo(lng), userDetails.IdUser, request.CheckIn, request.CheckOut, request.Rooms, request.TouristCount, request.ServiceId);
                }
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonHelper.ObjectToJsonLowerCamelCase(result));
            }
            catch (Exception ex)
            {
                var logger = ApplicationLogging.CreateLogger(LoggerType.Error);
                logger.LogError(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(CommonHelper.GetKagentDetailsFromToken(Request).IdUser, ex.Message)));
                await Response.WriteAsync(ex.Message);
            }
        }

        [Authorize(Roles = "SYSDBA_WebAis, SITE_FULL, SITE_VIEW")]
        [HttpPost]
        [Route("transferServices/{lng}")]
        public async Task transferServices([FromBody] TransferServicesRequest request, string lng)
        {
            try
            {
                var userDetails = CommonHelper.GetKagentDetailsFromToken(Request);
                string format = "yyyy-MM-dd";
                DateTime fromDate = DateTime.ParseExact(request.fromDate, format, CultureInfo.InvariantCulture);

                var result = repository.GetTransferServices(CultureInfo.GetCultureInfo(lng), userDetails, fromDate, request.cityId, request.transferCode);
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonHelper.ObjectToJsonLowerCamelCase(result));
            }
            catch (Exception ex)
            {
                var logger = ApplicationLogging.CreateLogger(LoggerType.Error);
                logger.LogError(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(CommonHelper.GetKagentDetailsFromToken(Request).IdUser, ex.Message)));
                await Response.WriteAsync(ex.Message);
            }
        }

        [Authorize(Roles = "SYSDBA_WebAis, SITE_FULL, SITE_VIEW")]
        [HttpPost]
        [Route("calculation")]
        public async Task calculation([FromBody] CalculationRequest request)
        {
            try
            {
                var userDetails = CommonHelper.GetKagentDetailsFromToken(Request);
                string format = "yyyy-MM-dd";
                DateTime checkIn = DateTime.ParseExact(request.checkIn, format, CultureInfo.InvariantCulture);
                DateTime checkOut = DateTime.ParseExact(request.checkOut, format, CultureInfo.InvariantCulture);
                var source = Enum.Parse<HotelSource>(request.source);
                if (source == HotelSource.travelline2)
                {
                    request.baseServiceId = CommonHelper.GetServiceIdFromString(request.baseServiceId);
                    request.serviceId = CommonHelper.GetServiceIdFromString(request.serviceId);
                }

                var result = repository.GetServiceDailyTariffs(userDetails, checkIn, checkOut, request.serviceId, request.baseServiceId, source, request.touristCount, request.hotelId, request.cityId, request.place, request.extBronIsNetto, request.price, request.quantity);
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonHelper.ObjectToJsonLowerCamelCase(result));
            }
            catch (Exception ex)
            {
                var logger = ApplicationLogging.CreateLogger(LoggerType.Error);
                logger.LogError(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(CommonHelper.GetKagentDetailsFromToken(Request).IdUser, ex.Message)));
                await Response.WriteAsync(ex.Message);
            }
        }

        [Authorize(Roles = "SYSDBA_WebAis, SITE_FULL, SITE_VIEW")]
        [HttpGet]
        [Route("visa/{lng}")]
        public async Task visa()
        {
            try
            {
                var userDetails = CommonHelper.GetKagentDetailsFromToken(Request);
                var result = repository.GetVisaServiceTariffPeriods(DateTime.Today, DateTime.Today, userDetails);
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonHelper.ObjectToJsonLowerCamelCase(result));
            }
            catch (Exception ex)
            {
                var logger = ApplicationLogging.CreateLogger(LoggerType.Error);
                logger.LogError(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(CommonHelper.GetKagentDetailsFromToken(Request).IdUser, ex.Message)));
                await Response.WriteAsync(ex.Message);
            }
        }

        [Authorize(Roles = "SYSDBA_WebAis, SITE_FULL, SITE_VIEW")]
        [HttpGet]
        [Route("VisaSpecial/{segmentId}")]
        [ResponseCache(VaryByHeader = "User-Agent", Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task GetVisaComments(int segmentId)
        {
            try
            {
                var result = repository.GetSegmentMessages(segmentId);
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonHelper.ObjectToJsonLowerCamelCase(result));
            }
            catch (Exception ex)
            {
                var logger = ApplicationLogging.CreateLogger(LoggerType.Error);
                logger.LogError(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(CommonHelper.GetKagentDetailsFromToken(Request).IdUser, ex.Message)));
                await Response.WriteAsync(ex.Message);
            }
        }

        [Authorize(Roles = "SYSDBA_WebAis, SITE_FULL, SITE_VIEW")]
        [HttpPost]
        [Route("VisaSpecial/{segmentId}/commentary/{lng}")]
        public async Task AddVisaComment([FromBody] MessageRequest request, int segmentId, string lng)
        {
            try
            {
                var userDetails = CommonHelper.GetKagentDetailsFromToken(Request);
                var result = repository.AddSegmentMessages(segmentId, request.message, HttpContext.Request.Host.Value, userDetails, lng);
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonHelper.ObjectToJsonLowerCamelCase(result));
            }
            catch (Exception ex)
            {
                var logger = ApplicationLogging.CreateLogger(LoggerType.Error);
                logger.LogError(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(CommonHelper.GetKagentDetailsFromToken(Request).IdUser, ex.Message)));
                await Response.WriteAsync(ex.Message);
            }
        }

        [Authorize(Roles = "SYSDBA_WebAis, SITE_FULL, SITE_VIEW")]
        [HttpGet]
        [Route("segment/{segmentId}/terms/{confirmation}/{lng}")]
        public async Task CreateTour(decimal segmentId, string confirmation, string lng)
        {
            try
            {
                var userDetails = CommonHelper.GetKagentDetailsFromToken(Request);
                Response.ContentType = "application/json";
                var result = new { terms = repository.GetServiceTerms(userDetails, lng, segmentId, confirmation) };
                await Response.WriteAsync(JsonHelper.ObjectToJsonLowerCamelCase(result));
            }
            catch (Exception ex)
            {
                var logger = ApplicationLogging.CreateLogger(LoggerType.Error);
                logger.LogError(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(CommonHelper.GetKagentDetailsFromToken(Request).IdUser, ex.Message)));

                var err = repository.GetTextError("DOWNLOAD_FILE_ERROR", CultureInfo.GetCultureInfo(lng));
                await Response.WriteAsync(JsonHelper.ObjectToJsonLowerCamelCase(new { terms = new string[0] }));
            }
        }

        /// <summary>
        /// ���������� ����������� ������������ ������ �� ��������� ����������
        /// </summary>
        [Authorize(Roles = "SYSDBA_WebAis, SITE_FULL, SITE_VIEW")]
        [HttpPost]
        [Route("checkFreeSale")]
        public async Task CheckFreeSale([FromBody] CheckFreeSaleRequest request, string lng)
        {
            try
            {
                var result = new { freeSale = repository.CheckFreeSale(request.HotelId, request.ServiceId, request.CheckIn, request.CheckOut, request.Place, request.Rooms) };
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonHelper.ObjectToJsonLowerCamelCase(result));
            }
            catch (Exception ex)
            {
                var logger = ApplicationLogging.CreateLogger(LoggerType.Error);
                logger.LogError(JsonHelper.ObjectToJsonLowerCamelCase(new GrayLogMessage(CommonHelper.GetKagentDetailsFromToken(Request).IdUser, ex.Message)));
                await Response.WriteAsync(ex.Message);
            }
        }
    }
}

